export interface Question {
	id : string
	type : string
	prompt : string
}

export interface YesNoQuestion extends Question {
	yes : { prompt : string, next : string}
	no : { prompt : string, next : string}
}

export interface QuestionOption {
	prompt : string,
	next : string,
	key : string,
}

export interface MultipleQuestion extends Question {
	options : QuestionOption[]
}

export interface NumberQuestion extends Question {
	min ?: number,
	max ?: number,
	unit ?: string,
	next : string,
}