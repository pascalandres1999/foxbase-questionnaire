import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  constructor(
    private questionService : QuestionService,
    private router : Router,
  ) { }

  ngOnInit(): void {
    console.log(this.questionService.getAnswers())
  }

  redo() {
    this.questionService.reset()
    this.router.navigate([''])
  }

}
