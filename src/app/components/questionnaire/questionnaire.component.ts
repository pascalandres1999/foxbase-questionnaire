import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators'
import { QuestionService } from 'src/app/services/question.service';
import { Question } from 'src/app/types/question';

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.scss']
})
export class QuestionnaireComponent implements OnInit {

  question$ : Observable<Question> = new Observable();

  constructor(
    private questionService : QuestionService,
    private router : Router,
  ) { }

  ngOnInit(): void {
    this.question$ = this.questionService.getStartingQuestion().pipe(
      switchMap(startId => this.questionService.getQuestion(startId))
    )
  }

  handleAnswer(answer : {
    id : string,
    value : string | number | boolean,
    next : string
  }) {
    this.questionService.answerQuestion(answer.id, answer.value);

    if(answer.next === 'end') {
      this.router.navigate(['result']);
    } else {
      this.question$ = this.questionService.getQuestion(answer.next);
    }
  }
}