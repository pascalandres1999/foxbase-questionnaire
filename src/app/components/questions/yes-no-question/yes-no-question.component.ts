import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

import { Question, YesNoQuestion } from 'src/app/types/question';

@Component({
  selector: 'app-question-yes-no',
  templateUrl: './yes-no-question.component.html',
  styleUrls: ['./yes-no-question.component.scss']
})
export class YesNoQuestionComponent implements OnInit {

  @Input('question') question : YesNoQuestion | undefined;
  @Output('answer') answer = new EventEmitter();

  constructor() { }

  ngOnInit(): void { }

}
