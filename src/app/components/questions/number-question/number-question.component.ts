import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms'
import { NumberQuestion } from '../../../types/question'

@Component({
  selector: 'app-question-number',
  templateUrl: './number-question.component.html',
  styleUrls: ['./number-question.component.scss']
})
export class NumberQuestionComponent implements OnInit {

  @Input('question') question : NumberQuestion | undefined;
  @Output('answer') answer = new EventEmitter();
  questionForm : FormGroup | undefined;

  constructor() { }

  ngOnInit(): void {

    let validators = [Validators.required]

    if(this.question?.min != undefined) {
      validators.push(Validators.min(this.question.min))
    }

    if(this.question?.max != undefined) {
      validators.push(Validators.max(this.question.max))
    }


    this.questionForm = new FormGroup({
      amount : new FormControl(this.amount, validators)
    })
  }

  get amount() {
    return this.questionForm?.get('amount')
  }

  onSubmit() {
    this.answer.emit({
      id : this.question?.id,
      value : this.amount?.value,
      next : this.question?.next
    })
  }

}
