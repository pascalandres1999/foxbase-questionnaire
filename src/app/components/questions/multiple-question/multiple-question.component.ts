import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { MultipleQuestion } from '../../../types/question'

@Component({
  selector: 'app-question-multiple',
  templateUrl: './multiple-question.component.html',
  styleUrls: ['./multiple-question.component.scss']
})
export class MultipleQuestionComponent implements OnInit {

  @Input('question') question : MultipleQuestion | undefined;
  @Output('answer') answer = new EventEmitter();

  constructor() { }

  ngOnInit(): void { }

}
