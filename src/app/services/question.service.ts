import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators'

import {
  Question, YesNoQuestion
} from '../types/question'

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  readonly configData : any = {
    questions : {
      "start" : "vegi",
      "vegi" : {
        id : "vegi",
        type : "yes_no",
        prompt : "Bist du Vegetarier?",
        yes : { prompt : "Ja", next : "vegan"},
        no : { prompt : "Nein", next : "selection_full"}
      },

      "vegan" : {
        id : "vegan",
        type : "yes_no",
        prompt : "Bist du Veganer?",
        yes : { prompt : "Ja", next : "selection_vegan"},
        no : { prompt : "Nein", next : "selection_vegi"},
      },

      "selection_full" : {
        id : "selection_full",
        type : "multiple",
        prompt : "Welche Art von Essen willst du?",
        options : [
          { key : "bread", prompt : "Brot", next : "diet"},
          { key : "meat", prompt : "Fleisch", next : "diet"},
          { key : "milk", prompt : "Milchprodukte", next : "diet"},
          { key : "vegetables", prompt : "Gemüse", next : "diet"},
          { key : "fruit", prompt : "Obst", next : "diet"},
        ]
      },

      "selection_vegi" : {
        id : "selection_vegi",
        type : "multiple",
        prompt : "Welche Art von Essen willst du?",
        options : [
          { key : "bread", prompt : "Brot", next : "diet"},
          { key : "milk", prompt : "Milchprodukte", next : "diet"},
          { key : "vegetables", prompt : "Gemüse", next : "diet"},
          { key : "fruit", prompt : "Obst", next : "diet"},
        ]
      },

      "selection_vegan" : {
        id : "selection_vegan",
        type : "multiple",
        prompt : "Welche Art von Essen willst du?",
        options : [
          { key : "bread", prompt : "Brot", next : "diet"},
          { key : "vegetables", prompt : "Gemüse", next : "diet"},
          { key : "fruit", prompt : "Obst", next : "diet"},
        ]
      },

      "diet" : {
        id : "diet",
        type : "yes_no",
        prompt : "Sind Sie grade auf Diät?",
        yes : { prompt : "Ja", next : "price"},
        no : { prompt : "Nein", next : "price"},
      },

      "price" : {
        id : "price",
        type : "number",
        prompt : "Was ist Ihr Preislimit?",
        unit : "€",
        min : 0,
        next : "end",
      }
    }
  }

  answers : Map<string, string | number | boolean> = new Map()

  constructor() { }

  getQuestion(id : string) : Observable<Question> {
    return of(this.configData.questions[id]).pipe(
      delay(100),
    );
  }

  getStartingQuestion() : Observable<string> {
    return of(this.configData.questions.start).pipe(
      delay(100),
    )
  }

  answerQuestion(id : string, answer : string | number | boolean) {
    this.answers.set(id, answer)
  }

  getAnswers() {
    return this.answers
  }

  reset() {
    this.answers = new Map();
  }
}
