import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QuestionnaireComponent } from './components/questionnaire/questionnaire.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { YesNoQuestionComponent } from './components/questions/yes-no-question/yes-no-question.component';
import { MultipleQuestionComponent } from './components/questions/multiple-question/multiple-question.component';
import { NumberQuestionComponent } from './components/questions/number-question/number-question.component';
import { StartComponent } from './components/start/start.component';
import { ResultComponent } from './components/result/result.component';

@NgModule({
  declarations: [
    AppComponent,
    QuestionnaireComponent,
    YesNoQuestionComponent,
    MultipleQuestionComponent,
    NumberQuestionComponent,
    StartComponent,
    ResultComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
