import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionnaireComponent } from './components/questionnaire/questionnaire.component';
import { ResultComponent } from './components/result/result.component';
import { StartComponent } from './components/start/start.component';

const routes: Routes = [
  {path : '', component : StartComponent},
  {path : 'questionnaire', component : QuestionnaireComponent},
  {path : 'result', component : ResultComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
