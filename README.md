# Questionnaire

To get this application started first clone this repository.
Make sure you have got `node` and `npm` installed.

Move into the cloned directory and run

```
npm install && npm start
```

After that visit `http://localhost:4200` in your browser.

The configuration of the example data can be found in `src/app/services/question.service.ts`.